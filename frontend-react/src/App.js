import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import './App.css'; 
import CalendarComponent from './components/calendar/calendar.component';

class App extends Component {
  render() {
    return (
     <Router>
       <React.Fragment>
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="/"><h1>MovieShows!!!</h1></a>
          </nav>
         <Switch>
		        <Route exact path="/" render={()=><Redirect to="/shows"/>} />
            <Route exact path="/shows" component={CalendarComponent}/> 
         </Switch>
			 </React.Fragment>
     </Router>
    );
  }
}

export default App;
