import React, {
	Component
} from 'react';
import $ from 'jquery';
import 'fullcalendar';
import moment from 'moment';

class CalendarComponent extends Component {

	constructor() {
		super();
		this.state = {
			showList: []
		};
	}

	componentDidMount() {
		this.fetchShowListFromApi();
		
	}

	componentDidUpdate() {
		let eventList = this.populateEventListForCalender();
		console.log("eventListConsole:", eventList);
		this.showCalender(eventList);
	}

	fetchShowListFromApi() {
		fetch('http://localhost:8080/api/shows')
			.then(res => res.json())
			.then((showListData) => {
				this.setState({
					showList: showListData
				});
				//  console.log(this.state);
			},
		(error)=>{
			if(error){
				alert("couldnt fetch data from node api!!!!");
			}

		});
	}

	populateEventListForCalender() {
		let eventList = this.state.showList.map((show, index) => {
			return {
				title: show.Movie.MovieName,
				start: show.ShowDateTime,
				theatre: show.Theatre.TheatreName,
				auditoriumName:show.Auditorium.AuditoriumName
			};
		});
		return eventList;
	}

	showCalender(eventList) {
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			editable: true,
			droppable: true,
			events: eventList,
			eventMouseover: function (data, event, view) {
				var tooltip = `<div class="tooltiptopicevent"				               
					style="width:auto;height:auto;background:#feb811;
					position:absolute;z-index:10001;padding:10px 10px 10px 10px ;  
					line-height: 200%;">
					  Movie:${data.title}  
					  </br>
					   Theatre:${data.theatre}
					   </br>
					  AuditoriumName:${data.auditoriumName}
				 	  </br>
					  start:${moment(data.start).format('h:mm:ss a')} 
					</div>`;


				$("body").append(tooltip);
				$(this).mouseover(function (e) {
					$(this).css('z-index', 10000);
					$('.tooltiptopicevent').fadeIn('500');
					$('.tooltiptopicevent').fadeTo('10', 1.9);
				}).mousemove(function (e) {
					$('.tooltiptopicevent').css('top', e.pageY + 10);
					$('.tooltiptopicevent').css('left', e.pageX + 20);
				});


			},
			eventMouseout: function (data, event, view) {
				$(this).css('z-index', 8);

				$('.tooltiptopicevent').remove();

			},
		})
	}


	render() {
		return <div id = "calendar" className = "container"></div>;
		 
	}
}

export default CalendarComponent;