var axios = require('axios');

function getShowsFromApi() {
    return new Promise((resolve, reject) => {
        axios.get('http://124.41.240.154:9803//api/nowshowinginfo')
            .then(function (response) {
                resolve(response.data);
            })
            .catch(function (error) {
                reject(error);
            });
    }); //close promise

}

module.exports = {
    getShowsFromApi
}