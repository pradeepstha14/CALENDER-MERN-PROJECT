var express = require('express');
var router = express.Router();
var ApiController = require('../controllers/api.controller');



router.get('/api/shows', ApiController.getShows);



module.exports = router;