var express = require('express');
var path = require('path');
var fs = require('fs');
var morgan = require('morgan')
var cors = require('cors');

//importing routes modules
var ApiRoute = require('./routes/api.route');


//for accessiing environment variables
require('dotenv').config();

//Init Express app
var app = express();

app.use(morgan("combined"));

app.use(cors({
  exposedHeaders: {
    "bodyLimit": "100kb",
    "corsHeaders": ["Link"]
  },
}));

app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));


//route middlewares
var routersPath = path.join(__dirname, "routes");
fs.readdirSync(routersPath).forEach(function (file) {
  app.use(require("./routes/" + file));
});


app.set('port', process.env.PORT || process.env.APP_PORT);
var server = app.listen(app.get('port'), function () {
  console.log('server is running at port = ' + server.address().port);
});


module.exports = app;