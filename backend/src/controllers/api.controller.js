var ApiService = require('../services/api.service');

function getShows(req, res, next) {
    ApiService.getShowsFromApi()
        .then((data) => res.status(200).json(data))
        .catch((error)=> next(error));
}


module.exports = {
    getShows
}